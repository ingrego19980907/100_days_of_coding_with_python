from turtle import Turtle

ALIGN = "center"
FONT = ("Arial", 20, "normal")


class ScoreBoard(Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        self.shape('triangle')
        self.hideturtle()
        self.penup()
        self.goto(x=0, y=260)
        self.color("white")

    def write_score(self):
        self.clear()
        self.write(f"Score = {self.score}", move=False, align=ALIGN, font=FONT)

    def increase_score(self):
        self.score += 1
        self.write_score()

    def game_over(self):
        self.goto(0, 0)
        self.write("Game Over", move=False, align=ALIGN, font=FONT)
