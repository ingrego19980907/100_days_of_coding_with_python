from art_higer_lower import logo, vs
from game_data import data
from random import choice
from termcolor import cprint



live = True
right_answers = 0
star_dict_1 = None

cprint(logo, color='yellow')

while live:
    if not star_dict_1:
        star_dict_1 = choice(data)
    print('Compare A: {}, {}, {}'.format(star_dict_1['name'], star_dict_1['description'], star_dict_1['country']))
    quantity_followers_1 = star_dict_1['follower_count']
    cprint(vs, color='cyan')
    star_dict_2 = choice(data)
    print('Compare B: {}, {}, {}'.format(star_dict_2['name'], star_dict_2['description'], star_dict_2['country']))
    quantity_followers_2 = star_dict_2['follower_count']
    answer = input("A or B \n")
    max_quan = quantity_followers_2 > quantity_followers_1
    if max_quan:
        if answer.lower() == 'b':
            right_answers += 1
            cprint(f"You're right! Current score: {right_answers}", color='green')
        else:
            cprint(f"Game Over. Your score {right_answers}", color='red')
            live = False
    else:
        if answer.lower() == 'a':
            right_answers += 1
            cprint(f"You're right! Current score: {right_answers}", color='green')
        else:
            cprint(f"Game Over. Your score {right_answers}", color='red')
            live = False
    star_dict_1 = star_dict_2

