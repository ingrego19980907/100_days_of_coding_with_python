from turtle import Turtle

POSITION_1 = [(340, 20), (340, 0), (340, -20)]
POSITION_2 = [(-340, 20), (-340, 0), (-340, -20)]
pos = [POSITION_1, POSITION_2]


class Paddle:

    def __init__(self, pos_index=0):
        self.segments = []
        self.paddle_body(pos_index=pos_index)

    def paddle_body(self, pos_index):
        for position in pos[pos_index]:
            new_segment = Turtle('square')
            new_segment.color('white')
            new_segment.penup()
            new_segment.goto(position)
            self.segments.append(new_segment)

    def move_down(self):
        for seg in self.segments:
            new_y = seg.ycor() - 20
            seg.goto(seg.xcor(), new_y)

    def move_up(self):
        for seg in self.segments:
            new_y = seg.ycor() + 20
            seg.goto(seg.xcor(), new_y)
