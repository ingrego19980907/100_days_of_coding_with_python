import  random

test_seed = int(input())
random.seed(test_seed)

random_side = random.randint(0, 1)
if random_side == 1:
    print('Tails')
else:
    print('Heads')
