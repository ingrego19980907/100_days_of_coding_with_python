from turtle import Turtle

COLORS = ["red", "orange", "yellow", "green", "blue", "purple"]
STARTING_MOVE_DISTANCE = 5
MOVE_INCREMENT = 10


class CarManager(Turtle):

    def __init__(self):
        from random import choice,  randint
        super().__init__()
        self.shape('square')
        self.penup()
        self.color(choice(COLORS))
        self.goto(300, randint(-240, 250))
        self.shapesize(1, 2)
        self.randnum = 1

    def move(self):
        from random import randint
        new_xcor = self.xcor() - randint(self.randnum, MOVE_INCREMENT)
        self.goto(new_xcor, self.ycor())
