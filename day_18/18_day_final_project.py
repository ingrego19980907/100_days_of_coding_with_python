# import colorgram
#
# rgb_colors = []
# colors = colorgram.extract('herst.jpg', 100)
# for color in colors:
#     r = color.rgb.r
#     g = color.rgb.g
#     b = color.rgb.b
#     rgb_colors.append((r, g, b))
#
# print(rgb_colors)
import turtle as t
from random import choice

timmy = t.Turtle()

colors_rgb = [(227, 225, 228), (236, 235, 240), (237, 229, 216), (212, 148, 97), (179, 66, 37), (15, 24, 43),
              (240, 208, 96), (232, 236, 234), (71, 29, 33), (234, 63, 34), (86, 94, 115), (158, 25, 20), (118, 33, 38),
              (157, 141, 74), (182, 86, 95), (138, 152, 164), (66, 44, 37), (168, 58, 63), (49, 52, 126),
              (231, 172, 160), (169, 144, 151), (214, 178, 182), (189, 189, 198), (75, 68, 48), (120, 122, 143),
              (102, 114, 113), (154, 165, 151), (15, 20, 19), (188, 193, 188), (56, 66, 68), (122, 130, 132)]

timmy.penup()
t.colormode(255)
x = -300
y = -230

for column in range(10):
    for row in range(11):
        timmy.goto(x + 60 * row, y + 60 * column)
        timmy.dot(30, choice(colors_rgb))

screen = t.Screen()
screen.exitonclick()
