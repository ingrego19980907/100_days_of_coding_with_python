

def prime_number(number_n):
    divisors = 0
    for n in range(2, number_n):
        if (number_n % n) == 0:
            divisors += 1

    if divisors == 0:
        print("Your number is prime")
    else:
        print(f"Your number isn't prime, it have {divisors} divisors")


print('Welcome to the prime number checking')
number = int(input('Please, enter your number\n'))
prime_number(number)
