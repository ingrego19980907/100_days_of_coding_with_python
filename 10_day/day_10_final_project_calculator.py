from art import logo


# Calculator


# Add


def add(n1, n2):
    return n1 + n2


# Subtract

def subtract(n1, n2):
    return n1 - n2


# Multiply

def multiply(n1, n2):
    return n1 * n2


# Divide

def divide(n1, n2):
    return n1 / n2


operations = {
    "+": add,
    "-": subtract,
    "*": multiply,
    "/": divide,
              }


def calculation():
    print(logo)
    answer = None
    should_continue = True

    while should_continue:
        if answer is None:
            num1 = float(input("Please, enter first number\n"))
        else:
            num1 = answer
        for symbol in operations:
            print(symbol)
        operation_symbol = input("please, enter operation\n")
        num2 = float(input("Please, enter second number\n"))
        function = operations[operation_symbol]
        answer = function(num1, num2)
        print(f"{num1} {operation_symbol} {num2} = {answer}")
        user_answer = input(f"do you want to continue calculating with {answer}? (y or n)"
                            f"enter c if you want to do new calculating operation\n")
        if user_answer == 'n':
            should_continue = False
        elif user_answer == 'c':
            calculation()


calculation()
