from turtle import Screen
from pong_angle import Paddle
from ball import Ball
import time
from scoreboard_for_pong import ScoreBoard

screen = Screen()
screen.setup(width=800, height=600)
screen.bgcolor('black')
screen.title('Pong Game')
screen.tracer(0)
paddle_1 = Paddle()
paddle_2 = Paddle(pos_index=1)
ball = Ball()
scoreboard = ScoreBoard()

screen.listen()
screen.onkey(paddle_1.move_up, "Up")
screen.onkey(paddle_1.move_down, "Down")
screen.onkey(paddle_2.move_up, "w")
screen.onkey(paddle_2.move_down, "s")

scoreboard.write_score()
game_is_on = True
while game_is_on:
    screen.update()
    time.sleep(0.07)
    ball.move(paddle_1.segments, paddle_2.segments)
    scoreboard.write_score()

    if ball.xcor() <= -400:
        scoreboard.increase_score_1_player()
        ball.goto(0, 0)
        time.sleep(0.5)
    elif ball.xcor() >= 400:
        scoreboard.increase_score_2_player()
        ball.goto(0, 0)
        time.sleep(0.5)

    if scoreboard.score_1_player == 10:
        scoreboard.win(1)
        game_is_on = False
    if scoreboard.score_2_player == 10:
        scoreboard.win(2)
        game_is_on = False


screen.exitonclick()
