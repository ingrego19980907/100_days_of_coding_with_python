from random import choice

rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

user_answer = input('Please, enter rock, paper or scissors\n')
pictures = [rock, scissors, paper]
if user_answer.lower() == 'rock':
    print(pictures[0])
elif user_answer.lower() == 'scissors':
    print(pictures[1])
else:
    print(pictures[2])

computer_answer = choice(['rock', 'scissors', 'paper'])
if computer_answer == 'rock':
    print(pictures[0])
elif computer_answer == 'scissors':
    print(pictures[1])
else:
    print(pictures[2])

if user_answer.lower() == computer_answer:
    print('Friendship won!')
if user_answer.lower() == 'rock' and computer_answer == 'scissors':
    print('You won!')
elif user_answer.lower() == 'scissors' and computer_answer == 'paper':
    print('You won!')
elif user_answer.lower() == 'paper' and computer_answer == 'rock':
    print('You won!')
else:
    print('You lose')
