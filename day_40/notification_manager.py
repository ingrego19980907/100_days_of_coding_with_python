from twilio.rest import Client

TWILIO_SID = "AC91c4d1818d936bc3a982f3a6fcaadace"
TWILIO_AUTH_TOKEN = "f2723b1f517031f04dc0698434d29c5f"
TWILIO_VIRTUAL_NUMBER = "+13012653798"
TWILIO_VERIFIED_NUMBER = "+380664979223"


class NotificationManager:

    def __init__(self):
        self.client = Client(TWILIO_SID, TWILIO_AUTH_TOKEN)

    def send_sms(self, message):
        message = self.client.messages.create(
            body=message,
            from_=TWILIO_VIRTUAL_NUMBER,
            to=TWILIO_VERIFIED_NUMBER,
        )
        # Prints if successfully sent.
        print(message.sid)
