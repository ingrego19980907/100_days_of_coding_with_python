import time
from random import randint, choice
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)

cars = []

scoreboard = Scoreboard()

player = Player()

screen.listen()
screen.onkey(player.move, 'Up')

list_probability = [1, 0, 0, 0, 0, 0, 0]

game_is_on = True
while game_is_on:
    scoreboard.write_score()
    time.sleep(0.1)
    screen.update()

    for _ in range(choice(list_probability)):
        car = CarManager()
        cars.append(car)

    for car in cars:
        car.move()

    for car in cars:
        if car.distance(player) < 20:
            scoreboard.game_over()
            game_is_on = False

    if player.ycor() >= 290:
        scoreboard.score += 1
        player.goto(player.xcor(), -295)
        time.sleep(1)
        if scoreboard.score % 2 == 0:
            for car in cars:
                car.randnum += 1
        if scoreboard.score == 10:
            list_probability.pop()


screen.exitonclick()
