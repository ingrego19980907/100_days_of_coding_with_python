import os
from twilio.rest import Client
import requests

STOCK_NAME = "TSLA"
COMPANY_NAME = "Tesla Inc"

STOCK_ENDPOINT = "https://www.alphavantage.co/query"
NEWS_ENDPOINT = "https://newsapi.org/v2/everything"

TWILIO_SID = os.environ["TWILIO_SID"]
TWILIO_AUTH_TOKEN = os.environ["TWILIO_AUTH_TOKEN"]
stock_api_key = os.environ["STOCK_API_KEY"]
news_api_key = os.environ["NEWS_API_KEY"]
NEWS_URL = f"https://newsapi.org/v2/everything?q=keyword&apiKey={news_api_key}"

# STEP 1: Use https://www.alphavantage.co/documentation/#daily
# When stock price increase/decreases by 5% between yesterday and the day before yesterday then print("Get News").

# Get yesterday's closing stock price.

params_stock = {
	"function": "TIME_SERIES_DAILY",
	"symbol": STOCK_NAME,
	"apikey": stock_api_key,
}

response_news = requests.get(STOCK_ENDPOINT, params=params_stock)
print(response_news.status_code)
data = response_news.json()["Time Series (Daily)"]
data_list = [(key, value) for (key, value) in data.items()]
yesterday_closing_stock_price = data_list[0][1]["4. close"]
print(yesterday_closing_stock_price)

# Get the day before yesterday's closing stock price

day_before_yesterday_closing_stock_price = data_list[1][1]["4. close"]
print(day_before_yesterday_closing_stock_price)

# Find the positive difference between 1 and 2.

difference = abs(float(yesterday_closing_stock_price) - float(day_before_yesterday_closing_stock_price))
print(difference)

# Work out the percentage difference in price between closing price yesterday and closing price the day before
# yesterday.

one_percent = float(yesterday_closing_stock_price) / 100
difference_percent = difference / one_percent
print(difference_percent, "%")


# If TODO4 percentage is greater than 5 then print("Get News").

if difference_percent > .5:
	params_news ={
		"qintitle": COMPANY_NAME,
		"apiKey": news_api_key,
	}
	
	response_news = requests.get(NEWS_ENDPOINT, params=params_news)
	articles = response_news.json()["articles"]
	first_tree_article = articles[:3]
	f_articles_list = [f"Headline: {article['title']}\n Brief: {article['description']}" for article in
	                   first_tree_article]
	for article in f_articles_list:
		client = Client(TWILIO_SID, TWILIO_AUTH_TOKEN)
		message = client.messages.create(body=article, from_="1 857 763 3822", to="+380664979223")
	

# STEP 2: https://newsapi.org/
# Instead of printing ("Get News"), actually get the first 3 news pieces for the COMPANY_NAME.

# Instead of printing ("Get News"), use the News API to get articles related to the COMPANY_NAME.


# - Use Python slice operator to create a list that contains the first 3 articles.

# STEP 3: Use twilio.com/docs/sms/quickstart/python
# to send a separate message with each article's title and description to your phone number.
# Create a new list of the first 3 article's headline and description using list comprehension.
# Send each article as a separate message via Twilio.



#Optional  Format the message like this:


"""
TSLA: 🔺2%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
or
"TSLA: 🔻5%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
"""

