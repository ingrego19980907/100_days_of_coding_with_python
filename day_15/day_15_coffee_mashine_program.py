
class CoffeeMachine:

    def __init__(self):
        self.money = 0
        self.water = 300
        self.milk = 200
        self.coffee = 100
        self.what_drink_do = None
        self.drinks_and_prises = {'espresso': 1.5, 'latte': 2.5, 'cappuccino': 2.5}
        self.answer = None
        self.coins_calculated = None

    def ask_what_do(self):
        self.answer = input("What would you like? (espresso/latte/cappuccino)\n")
        self.answer = self.answer.lower()

    def act(self):
        while True:
            self.ask_what_do()
            if self.answer in self.drinks_and_prises:
                print(f'It costs {self.drinks_and_prises[self.answer.lower()]}$')
                if self.check_drink_and_resources():
                    self.count_coins()
                    if self.check_transaction():
                        self.make_coffee()
                    else:
                        print("Insufficient money!")

            elif self.answer == 'report':
                self.report()
            elif self.answer == 'off':
                self.off_machine()
            else:
                print("You entered an incorrect word")
                Exception()

    def report(self):
        print("Water: {}ml\nMilk: {}ml\nCoffee: {}\nMoney: {}$".format(self.water, self.milk, self.coffee, self.money))

    def check_drink_and_resources(self):

        if self.answer == 'latte':
            self.what_drink_do = Latte()
        elif self.answer == 'cappuccino':
            self.what_drink_do = Cappuccino()
        elif self.answer == 'espresso':
            self.what_drink_do = Espresso()

        if self.water > self.what_drink_do.water and self.coffee > self.what_drink_do.coffee and self.milk >\
                self.what_drink_do.milk:
            return True
        else:
            print("There is not enough resources")
            return False

    def count_coins(self):
        quarters = int(input("Give the quarters"))
        dimes = int(input("give the dimes"))
        nickles = int(input("Give the nickles"))
        pennies = int(input("Give the pennies"))
        coins_calculate = quarters * .25 + dimes * .10 + nickles * .05 + pennies * .01
        self.coins_calculated = coins_calculate
        print("You payed {}$".format(self.coins_calculated))

    def check_transaction(self):
        if self.coins_calculated >= self.drinks_and_prises[self.answer]:
            print("Your delivery {}".format(self.coins_calculated - self.drinks_and_prises[self.answer]))
            return True
        else:
            return False

    def make_coffee(self):
        self.water -= self.what_drink_do.water
        self.milk -= self.what_drink_do.milk
        self.coffee -= self.what_drink_do.coffee
        self.money += self.drinks_and_prises[self.answer.lower()]
        print("Here is your {}. Enjoy!".format(self.answer))

    @staticmethod
    def off_machine():
        quit()


class Latte:

    def __init__(self):
        self.water = 200
        self.milk = 150
        self.coffee = 24


class Espresso:

    def __init__(self):
        self.water = 50
        self.milk = 0
        self.coffee = 18


class Cappuccino:
    def __init__(self):
        self.water = 250
        self.milk = 100
        self.coffee = 24


wnd3 = CoffeeMachine()
wnd3.act()
