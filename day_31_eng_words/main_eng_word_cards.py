from tkinter import *
import pandas as p
from numpy.random import choice

BACKGROUND_COLOR = "#B1DDC6"
n = 0
translate = None
en_word = None
# ---------------------------Gen word------------------


def change_img_word_en():
    canvas.itemconfig(canvas_img, image=image_1)
    canvas.itemconfig(english_text, text="English")


def change_img_word_ru():
    canvas.itemconfig(canvas_img, image=image_2)
    canvas.itemconfig(english_text, text="Russian")


def generate_rand_word_with_trans():
    global translate, en_word
    data = p.read_csv('data/en_5k.csv')
    dicts_w = data.to_dict(orient='records')
    rand_word = choice(dicts_w)
    en_word = rand_word['en']
    translate = rand_word['ru']


def next_card():
    global flip_timer
    window.after_cancel(flip_timer)
    generate_rand_word_with_trans()
    change_img_word_en()
    canvas.itemconfig(word, text=f"{en_word}")
    flip_timer = window.after(3000, flip_card)


def flip_card():
    change_img_word_ru()
    canvas.itemconfig(word, text=f"{translate}")


def passed_the_button_ok():
    with open('words_I_know.txt', 'a', encoding='utf-8') as f:
        string = f"{en_word}   {translate}\n"
        f.write(string)
    with open('data/en_5k.csv', 'r+', encoding='utf-8') as f:
        d = f.readlines()
        f.seek(0)
        for i in d:
            if i != f"{en_word},{translate}\n":
                f.write(i)
        f.truncate()
    next_card()


def passed_the_button_x():
    with open('words_I_dont_know.txt', 'a', encoding='utf-8') as f:
        string = f"{en_word}   {translate}\n"
        f.write(string)
    next_card()


# ------------------- UI-----------------------
window = Tk()

window.title('Learn English words!')
window.config(padx=50, pady=50, bg=BACKGROUND_COLOR)

canvas = Canvas(width=800, height=526, bg=BACKGROUND_COLOR, highlightthickness=0)
image_1 = PhotoImage(file='images/card_front.png')
image_2 = PhotoImage(file='images/card_back.png')
canvas_img = canvas.create_image(400, 261, image=image_1)
english_text = canvas.create_text(400, 150, text='English', font=('Arial', 40, 'italic'))
word = canvas.create_text(400, 263, text='Get_ready_3_sec!', font=('Arial', 60, 'bold'))
canvas.grid(column=0, row=0, columnspan=2)


button_x_img = PhotoImage(file='images/wrong.png')
button_x = Button(image=button_x_img, highlightthickness=0, command=passed_the_button_x)
button_x.grid(column=0, row=1)

button_ok_img = PhotoImage(file='images/right.png')
button_ok = Button(image=button_ok_img, highlightthickness=0, command=passed_the_button_ok)
button_ok.grid(column=1, row=1)


flip_timer = window.after(3000, next_card)














window.mainloop()