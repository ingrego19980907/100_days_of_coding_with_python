﻿import requests
from twilio.rest import Client
import os

API_KEY = "1a35b8f4a1fd1d191933bc06a1ba7058"
LATITUDES = 46.650452  # Detroid 42.332939 Kherson 46.650452
LONGITUDE = 32.608181         # -83.047836         32.608181
URL = f"https://api.openweathermap.org/data/2.5/onecall"

account_sid = os.environ['TWILIO_ACCOUNT_SID']
auth_token = os.environ['TWILIO_AUTH_TOKEN']

weather_params = {
	"lat": LATITUDES,
	"lon": LONGITUDE,
	"appid": API_KEY,
	"exclude": "current,minutely,dayly",
}

response = requests.get(URL, params=weather_params)
response.raise_for_status()

weather_data = response.json()
hourly_data = weather_data["hourly"]
bring_an_umbrella = None
massage_text = ""
for i in range(12):
	if hourly_data[i]["weather"][0]["id"] < 700:
		massage_text += f"in {i} hours {hourly_data[i]['weather'][0]['description']}\n"
		bring_an_umbrella = "Bring an umbrella!"

if bring_an_umbrella:
	client = Client(account_sid, auth_token)
	message = client.messages.create(
		body=f"{massage_text}\n {bring_an_umbrella} ☂️",
		from_='+18577633822',
		to='+380664979223'
	)
	print(message.sid)