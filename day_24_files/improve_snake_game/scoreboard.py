from turtle import Turtle

ALIGN = "center"
FONT = ("Arial", 20, "normal")


class ScoreBoard(Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        with open('data.txt', 'r') as f:
            num_str = f.read()
            num = int(num_str)
            self.high_score = num
        self.shape('triangle')
        self.hideturtle()
        self.penup()
        self.goto(x=0, y=260)
        self.color("white")

    def write_score(self):
        self.clear()
        self.write(f"Score = {self.score} High score = {self.high_score}", align=ALIGN, font=FONT)

    def increase_score(self):
        self.score += 1
        self.write_score()

    def reset(self):
        if self.score > self.high_score:
            self.high_score = self.score
            with open('data.txt', 'w') as f:
                f.write(str(self.high_score))
        self.score = 0
        self.write_score()

    # def game_over(self):
    #     self.goto(0, 0)
    #     self.write("Game Over", move=False, align=ALIGN, font=FONT)
