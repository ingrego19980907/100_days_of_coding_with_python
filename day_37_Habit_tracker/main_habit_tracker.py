﻿import requests
from datetime import datetime

USERNAME = "viacheslav"
TOKEN = "jdaskjfoeihfkldfjasoifwehggnsklaf"

pixela_endpoint = "https://pixe.la/v1/users"

user_params = {
	"token": TOKEN,
	"username": USERNAME,
	"agreeTermsOfService": "yes",
	"notMinor": "yes",
}

# Create user
# response = requests.post(url=pixela_endpoint, json=user_params)
# print(response.text)


# Create Graph
graph_endpoint = f"{pixela_endpoint}/{USERNAME}/graphs"
#
# graph_config = {
# 	"id": "graph1",
# 	"name": "Drawing graph",
# 	"unit": "min",
# 	"type": "int",
# 	"color": "sora",
# }
#
# headers = {
# 	"X-USER-TOKEN": TOKEN,
# }
#
#
# response = requests.post(graph_endpoint, json=graph_config, headers=headers)
# print(response.text)


endpoint_graph1 = f"{pixela_endpoint}/{USERNAME}/graphs/graph1"


today = datetime.now()
today_str = today.strftime("%Y%m%d")

headers = {
	"X-USER-TOKEN": TOKEN,
}

graph1_config = {
	"date": today_str,
	"quantity": "10",
	
}

response = requests.post(endpoint_graph1, json=graph1_config, headers=headers)
print(response.text)
