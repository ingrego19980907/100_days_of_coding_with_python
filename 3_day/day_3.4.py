print('Welcome to Python Pizza Deliveries')
size = input('What size pizza do you want? S, M, L \n')
add_pepperoni = input('Do you want pepperoni? Y, N\n')
extra_cheese = input('Do you want extra cheese? Y, N\n')
bill = 0
if size == 'S':
    bill += 15
elif size == 'M':
    bill += 20
elif size == 'L':
    bill += 25
else:
    print(' you enter incorrect answer')

if add_pepperoni == 'Y':
    if size == 'S':
        bill += 2
    elif size == 'M' or 'L':
        bill += 3

if extra_cheese == 'Y':
    bill += 1
print(f'your total bill is ${bill}')
