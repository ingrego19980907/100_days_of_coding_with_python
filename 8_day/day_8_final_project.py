from termcolor import cprint
from art import logo


def encrypt_and_decrypt(user_direction, plain_text, shift_n):

    if shift_n > (len(alphabet) - 1):
        shift_n = shift_n % len(alphabet)

    if user_direction == 'encode':
        output_text = ''

        for letter in plain_text:
            if letter in alphabet:
                index = alphabet.index(letter)
                if index + shift_n <= len(alphabet):
                    output_text += alphabet[index + shift_n]
                else:
                    output_text += alphabet[index + shift_n - len(alphabet)]
            else:
                output_text += letter

        print(f"Your text encrypted:  {output_text}")

    elif user_direction == 'decode':
        output_text = ''

        for letter in plain_text:
            if letter in alphabet:
                index = alphabet.index(letter)
                output_text += alphabet[index - shift_n]
            else:
                output_text += letter
        print(f"Your text decrypted:  {output_text}")

    else:
        cprint("Please, enter the correct text!", color='red')


alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

print(logo)

while True:
    direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")
    text = input("Type your message:\n").lower()
    shift = int(input("Type the shift number:\n"))
    encrypt_and_decrypt(user_direction=direction, plain_text=text.lower(), shift_n=shift)
    answer = input('Do you want to continue? yes or no\n')
    if answer.lower() == 'no':
        cprint("Good lack!", color='yellow')
        break

