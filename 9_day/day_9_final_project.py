from art import logo


def find_highest_bidder(bidding_recorde):
    highest_bid = 0
    winner = ""
    for bidder_name in bidding_recorde:
        bid = bidding_recorde[bidder_name]
        if bid > highest_bid:
            winner = bidder_name
            highest_bid = bid
    print(f"The winner is {winner} with a bid of {highest_bid}")


print(logo)

bids = {}
while True:
    name = input("What is your name?\n")
    price = int(input("What is your bid? \n$"))
    bids[name] = price
    answer = input("Are there others bidders?  yes or no")
    if answer.lower() == 'no':
        break
    elif answer.lower() == 'yes':
        print("\n"*50)

find_highest_bidder(bids)
