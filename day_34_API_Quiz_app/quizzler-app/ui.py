from tkinter import *
from quiz_brain import QuizBrain


THEME_COLOR = "#375362"


class QuizInterface:

    def __init__(self, quiz_brain: QuizBrain):
        self.quiz = quiz_brain

        self.window = Tk()
        self.window.title("Quizzer")
        self.window.config(padx=20, pady=20, bg=THEME_COLOR)
        x_image = PhotoImage(file='images/false.png')
        ok_image = PhotoImage(file='images/true.png')
        self.x_button = Button(image=x_image, highlightthickness=0,  command=self.click_on_button_x)
        self.ok_button = Button(image=ok_image, highlightthickness=0,  command=self.click_on_button_ok)

        self.score = Label(text="Score: 0", font=('Arial', 15), fg='white', bg=THEME_COLOR)
        self.score_text = 0
        self.canvas = Canvas(width=300, height=250, highlightthickness=0)
        self.question_text = self.canvas.create_text(150, 125, width=280, text="input", font=('Arial', 20, 'italic'))
        self.score.grid(column=1, row=0)
        self.canvas.grid(column=0, row=1, columnspan=2, pady=50)

        self.x_button.grid(column=1, row=2)
        self.ok_button.grid(column=0, row=2)
        self.get_next_question()

        self.window.mainloop()

    def get_next_question(self):
        self.canvas.config(bg='white')
        q_text = self.quiz.next_question()
        self.canvas.itemconfig(self.question_text, text=q_text)

    def click_on_button_ok(self):
        is_right = self.quiz.check_answer("True")
        self.give_feedback(is_right)

    def click_on_button_x(self):
        is_right = self.quiz.check_answer("False")
        self.give_feedback(is_right)

    def give_feedback(self, is_right):
        if is_right:
            self.canvas.config(bg='green')
            self.score_text += 1
            self.score.config(text="Score: {}".format(self.score_text))
        else:
            self.canvas.config(bg='red')
        self.window.after(1000, self.get_next_question)
