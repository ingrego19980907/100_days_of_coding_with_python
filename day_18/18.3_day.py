import turtle as t
from random import randint


def random_color():
    r = randint(0, 255)
    b = randint(0, 255)
    g = randint(0, 255)
    return r, g, b


t.colormode(255)
t.pensize(10)
t.speed("fastest")
timmy = t.Turtle()

for _ in range(200):
    timmy.circle(100, 360)
    timmy.left(5)
    timmy.color(random_color())

screen = t.Screen()
screen.exitonclick()
