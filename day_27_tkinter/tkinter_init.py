from tkinter import *

window = Tk()

window.title("My first GUI program!")
window.minsize(width=500, height=300)


my_label = Label(text="I am Label", font=("Arial", 24, "bold"))
my_label.grid(column=0, row=0)

my_label['text'] = '----'


def button_clicked():
    print("I got button")
    my_label.config(text="hi!")


button = Button(text='button', command=button_clicked)
button.grid(column=1, row=1)


# Entry


def click_on_button():
    name = input_t.get()
    my_label.config(text=f"hi! {name}")
    return name


input_t = Entry()
input_t.grid(column=3, row=2)
button_2 = Button(text="Input name", command=click_on_button)
button_2.grid(column=2, row=0)


window.mainloop()
