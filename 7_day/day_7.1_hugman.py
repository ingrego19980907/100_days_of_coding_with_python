from termcolor import cprint
from random import choice


def output_list(our_list):
    print_char_ = ''
    for char_i in our_list:
        print_char_ += (char_i + ' ')
    print(print_char_)


def your_live():
    if live > 0:
        cprint(f'You have {live} lives', color='yellow')
    else:
        cprint("Game Over", color='red')


live = 5
words = ['camel', 'banana', 'beautiful', 'father', 'hungry', 'loops', 'glasses']
print_ = []

word = choice(words)
for character in word:
    print_ += '_'

output_list(print_)

while live > 0:
    char = input('Please, enter letter:\n')
    if char in word:
        for i in range(len(word)):
            if char == word[i]:
                print_[i] = char
    else:
        live -= 1
    output_list(print_)
    your_live()
    if "_" not in print_:
        cprint('You won!', color='green')
        break
