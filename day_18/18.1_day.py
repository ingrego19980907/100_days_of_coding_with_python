from turtle import Turtle, Screen
from random import choice


def paint_figure(n):
    angel = 360/n
    for _ in range(n):
        timmy.forward(100)
        timmy.right(angel)


timmy = Turtle()
timmy.color("green")
# for _ in range(4):
#     timmy.forward(100)
#     timmy.right(90)
colors = ['red', 'blue', 'green', 'cyan', 'yellow', 'black']

for i in range(3, 10):
    paint_figure(i)
    timmy.color(choice(colors))
screen_1 = Screen()
screen_1.exitonclick()

