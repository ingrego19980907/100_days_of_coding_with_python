from tkinter import *


def click_on_button():
    mile = float(input_mile.get())
    km_convert = mile * 1.60934
    answer_num.config(text=f'{round(km_convert, 3)}')


window = Tk()

window.title('Mile to kilometers Converter')
window.minsize(width=300, height=200)
window.config(padx=40, pady=40)

answer_text = Label(text='is equal to', font=('Arial', 16))
answer_text.grid(column=0, row=1)

input_mile = Entry(width=10, font=('Arial', 16))
input_mile.grid(column=1, row=0)

answer_num = Label(text='0', font=('Arial', 16))
answer_num.grid(column=1, row=1)

miles_text = Label(text='Miles', font=('Arial', 16))
miles_text.grid(column=2, row=0)

km_text = Label(text='Km', font=('Arial', 16))
km_text.grid(column=2, row=1)

button = Button(text="calculate", font=('Arial', 16), command=click_on_button)
button.grid(column=1, row=2)

window.mainloop()
