from math import ceil


def paint_calc(height_w, width_w, cover):
    cans = ceil(height_w * width_w / cover)
    print("You'll need {} cans of paint".format(cans))


height = float(input('Please, enter height of the wall\n'))
width = float(input('Please, enter width of the wall\n'))
coverage = 5

paint_calc(height_w=height, width_w=width, cover=coverage)
