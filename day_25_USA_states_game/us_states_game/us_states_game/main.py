import turtle
import pandas

screen = turtle.Screen()
screen.title('U.S. States Game')

image = 'blank_states_img.gif'
screen.addshape(image)
turtle.shape(image)

FONT = ("Courier", 14, "normal")
ALIGN = "center"

# def get_mouse_click_coor(x, y):
#     with open('coordinate.txt', 'a') as f:
#         f.write(f'{x} {y}\n')
#
# turtle.onscreenclick(get_mouse_click_coor)
# turtle.mainloop()

class Name_states(turtle.Turtle):

    def __init__(self, name):
        super().__init__()
        self.name = name
        self.shape('triangle')
        self.hideturtle()
        self.penup()
        self.color("black")

    def write_state(self):
        self.write(f"{self.name}", move=False, align=ALIGN, font=FONT)


data = pandas.read_csv('50_states.csv')
data_states = data['state']

list_states = [state for state in data_states]
# for state in data_states:
#     list_states.append(state)


correct_answer = 0

while correct_answer <= 50:
    answer_state = screen.textinput(title=f"Guest the State {correct_answer}/50", prompt="What's another state's name?")

    if answer_state.lower() == 'exit':
        with open('States_to_learn.txt', 'w') as f:
            for state in list_states:
                f.write(state + '\n')
        break

    with open('50_states.csv', 'r') as f:
        for line in f:
            line = line.split(',')
            if answer_state.title() in line:
                correct_answer += 1
                state_name = str(line[0])
                x = int(line[1])
                y = int(line[2])
                state = Name_states(state_name)
                state.goto(x, y)
                state.write_state()
                if state_name in list_states:
                    list_states.remove(state_name.title())






