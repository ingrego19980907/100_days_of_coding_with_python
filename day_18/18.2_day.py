from random import randint, choice
import turtle as t


def random_color():
    r = randint(0, 255)
    b = randint(0, 255)
    g = randint(0, 255)
    return r, g, b


timmy = t.Turtle()
t.colormode(255)
step = 20
angel_1 = 90
colors = ['red', 'blue', 'green', 'cyan', 'yellow', 'black']
timmy.width(20)
directions = [0, 90, 180, 270]
timmy.pensize(15)
timmy.speed("fastest")

for _ in range(200):
    timmy.color(random_color())
    timmy.forward(20)
    timmy.setheading(choice(directions))




