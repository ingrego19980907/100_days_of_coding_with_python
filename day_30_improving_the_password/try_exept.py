try:
    file = open('a_file.txt')
    d = {'d': 3}
    print(d['d'])
except FileNotFoundError:
    file = open('a_file.txt', 'w')
except KeyError as error_message:
    print(f'{error_message} there is not key')
else:
    content = file.read()
    print(content)
finally:
    file.close()
    print('file was closed')


facebook_posts = [
    {'Likes': 21, 'Comments': 2},
    {'Likes': 13, 'Comments': 2, 'Shares': 1},
    {'Likes': 33, 'Comments': 8, 'Shares': 3},
    {'Comments': 4, 'Shares': 2},
    {'Comments': 1, 'Shares': 1},
    {'Likes': 19, 'Comments': 3}
]

total_likes = 0

for post in facebook_posts:
    try:
        total_likes = total_likes + post['Likes']
    except KeyError:
        continue


print(total_likes)