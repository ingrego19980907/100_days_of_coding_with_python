
#for each name in invited_names.txt
#Replace the [name] placeholder with the actual name.
#Save the letters in the folder "ReadyToSend".
    
#Hint1: This method will help you: https://www.w3schools.com/python/ref_file_readlines.asp
    #Hint2: This method will also help you: https://www.w3schools.com/python/ref_string_replace.asp
        #Hint3: THis method will help you: https://www.w3schools.com/python/ref_string_strip.asp

def write_line_in_file(name, line):
    with open(f'./Output/ReadyToSend/letter_{name}.txt', 'a+') as f:
        f.write(line + '\n')


with open('Input/Names/invited_names.txt', 'r') as names:
    list_names = []
    for name in names:
        name_s = name.strip('\n')
        list_names.append(name_s)
    for name in list_names:
        with open('Input/Letters/starting_letter.docx', 'r') as letter:
            for line in letter:
                line = line.replace('[name]', name)
                line = line.replace('Angela', 'Viatcheslav')
                line_s = line.strip('\n')
                write_line_in_file(name, line_s)
