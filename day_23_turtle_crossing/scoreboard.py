from turtle import Turtle

FONT = ("Courier", 24, "normal")
ALIGN = "center"


class Scoreboard(Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        self.shape('triangle')
        self.hideturtle()
        self.penup()
        self.goto(x=0, y=260)
        self.color("red")

    def write_score(self):
        self.clear()
        self.write(f"Level = {self.score}", move=False, align=ALIGN, font=FONT)

    def increase_score(self):
        self.score += 1
        self.write_score()

    def game_over(self):
        self.goto(0, 0)
        self.write("Game Over", move=False, align=ALIGN, font=FONT)
