from random import randint
from art_for_guess_num import logo

print(logo)
print("Welcome to the Guest number game!")
print("I'm think of a number between 1 and 100")

mode = input("Chose a difficulty. Entre 'hard' or 'normal'\n")

live = 0
if mode == 'hard':
    live = 5
elif mode == 'normal':
    live = 10
else:
    print("You enter incorrect word!")
    Exception()

rand_number = randint(1, 100)

while live > 0:
    print(f"You have {live} attempts remaining to guess the number")
    number = int(input("Make a guess:"))
    if number == rand_number:
        print(f"You guest! Number is {number}")
        break
    elif number > rand_number:
        print("Too high")
    else:
        print("Too low")
    live -= 1
    if live == 0:
        print("Game Over!")
