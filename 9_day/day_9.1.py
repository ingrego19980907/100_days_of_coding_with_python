student_scores = {
  "Harry": 81,
  "Ron": 78,
  "Hermione": 99, 
  "Draco": 74,
  "Neville": 62,
}

student_scores_crit = {}
for key in student_scores:
    if student_scores[key] > 90:
        score_crit = 'Outstanding'
    elif student_scores[key] > 80:
        score_crit = 'Exceeds Expectations'
    elif student_scores[key] > 70:
        score_crit = 'Acceptable'
    else:
        score_crit = 'So bed!!!'
    student_scores_crit[key] = score_crit

print(student_scores_crit)
