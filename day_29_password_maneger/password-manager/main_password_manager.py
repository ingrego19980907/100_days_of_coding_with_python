from tkinter import *
from random import choice
from password_data import all_data
from tkinter import messagebox
import json
# ---------------------------- PASSWORD GENERATOR ------------------------------- #

LENGTH = 20


def generate_password():
    password_entry.delete(0, 'end')
    password = ''
    for _ in range(LENGTH):
        new_char = choice(all_data)
        password += new_char
    password_entry.insert(0, password)

# ---------------------------- SAVE PASSWORD ------------------------------- #


def write_in_file():
    website = website_entry.get()
    email = email_entry.get()
    password = password_entry.get()
    new_data = {
        website: {
            "email": email,
            "password": password,

        }
    }
    if email == '' or password == '' or website == '':
        messagebox.showerror(title="Error", message="Don't enough data")
    else:
        try:
            with open('password_entry.json', 'r') as data_file:
                data = json.load(data_file)
                data.update(new_data)
            with open('password_entry.json', 'w') as data_file:
                json.dump(data, data_file, indent=4)
        except FileNotFoundError:
            with open('password_entry.json', 'w') as data_file:
                json.dump(new_data, data_file, indent=4)
        finally:
            website_entry.delete(0, 'end')
            password_entry.delete(0, 'end')
            messagebox.showinfo(title='Data added!', message='Your login and password added!')


def searching():
    try:
        website_search = website_entry.get()
        with open('password_entry.json', 'r') as data_file:
            data = json.load(data_file)
            for website in data:
                if website == website_search:
                    messagebox.showinfo(title='Your data founded',message=f'Email: {data[website]["email"]}\n'
                                                                          f'Password: {data[website]["password"]}')
                    break
            else:
                messagebox.showerror(title='Error', message="Data don't found")
    except FileNotFoundError:
        messagebox.showerror(title='Error', message="Data don't found")


# ---------------------------- UI SETUP ------------------------------- #


window = Tk()
window.title("Password Manager")
window.config(padx=20, pady=20)


canvas_1 = Canvas(width=200, height=200, highlightthickness=0)
img_logo = PhotoImage(file='logo.png')
canvas_1.create_image(100, 100, image=img_logo)
canvas_1.grid(column=1, row=0)

label_website = Label(text="Website:")
label_website.grid(column=0, row=1)

label_email = Label(text='Email/Username:')
label_email.grid(column=0, row=2)

label_password = Label(text='Password:')
label_password.grid(column=0, row=3)

website_entry = Entry(width=22, font=('Arial', 12))
website_entry.grid(column=1, row=1, sticky=W)
website_entry.focus()

email_entry = Entry(width=35, font=('Arial', 12))
email_entry.grid(column=1, row=2, columnspan=2, sticky=W)
email_entry.insert(0, 'suhodoiev@gmail.com')

password_entry = Entry(width=22, font=('Arial', 12))
password_entry.grid(column=1, row=3, sticky=W)


button_search = Button(text='Search', command=searching)
button_search.grid(column=2, row=1, sticky=W)

button_gen_passw = Button(text='Generate Password', command=generate_password)
button_gen_passw.grid(column=2, row=3, sticky=W)

button_add = Button(width=44, text="Add", command=write_in_file)
button_add.grid(column=1, row=4, columnspan=2, sticky=W)

window.mainloop()
