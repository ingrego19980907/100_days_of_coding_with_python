
from turtle import Turtle


ALIGN = "center"
FONT = ("Arial", 20, "normal")


class ScoreBoard(Turtle):

    def __init__(self):
        super().__init__()
        self.score_1_player = 0
        self.score_2_player = 0
        self.shape('triangle')
        self.hideturtle()
        self.penup()
        self.goto(x=0, y=260)
        self.color("white")

    def write_score(self):
        self.clear()
        self.write(f"Score  {self.score_1_player} | {self.score_2_player}", move=False, align=ALIGN, font=FONT)

    def increase_score_1_player(self):
        self.score_1_player += 1
        self.write_score()

    def increase_score_2_player(self):
        self.score_1_player += 1
        self.write_score()

    def win(self, num):
        self.goto(0, 0)
        self.write(f"Player {num} had won!", move=False, align=ALIGN, font=FONT)
