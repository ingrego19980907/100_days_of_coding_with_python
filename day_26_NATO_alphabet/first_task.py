list = [x * 2 for x in range(1, 5)]
print(list)
names = ['Cat', 'Po', 'Viatcheslav', 'Sasha']

names_sort = [name.upper() for name in names if len(name) > 3]
print(names_sort)


numbers = [1, 1, 2, 4, 5, 34, 55]

list_com = [num for num in numbers if not num % 2]
print(list_com)


with open('file1.txt', 'r') as f1, open('file2.txt', 'r') as f2:
    l1 = []
    for num in f1:
        num = num.strip('\n')
        l1.append(int(num))
    l2 = []
    for num in f2:
        num = num.strip('\n')
        l2.append(int(num))
    result = [x for x in l1 if x in l2]

print(result)


words = 'What is your name ?'
list_words = words.split()
dict_len_words = {word: len(word) for word in list_words}
print(dict_len_words)
