from turtle import Turtle, Screen

timmy = Turtle()
screen_1 = Screen()


def move_forwards():
    timmy.forward(10)


def move_backwards():
    timmy.back(10)


def turn_right():
    new_heading = timmy.heading() - 10
    timmy.setheading(new_heading)


def turn_left():
    new_heading = timmy.heading() + 10
    timmy.setheading(new_heading)


def clear():
    timmy.clear()
    timmy.penup()
    timmy.home()
    timmy.pendown()


screen_1.listen()
screen_1.onkey(key='w', fun=move_forwards)
screen_1.onkey(key='s', fun=move_backwards)
screen_1.onkey(key='d', fun=turn_right)
screen_1.onkey(key='a', fun=turn_left)
screen_1.onkey(key='c', fun=clear)

screen_1.exitonclick()
