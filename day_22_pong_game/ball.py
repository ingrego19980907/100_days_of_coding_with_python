from turtle import Turtle

class Ball(Turtle):

    def __init__(self):
        super(Ball, self).__init__()
        self.shape('circle')
        self.color('white')
        self.penup()
        self.step_x = 10
        self.step_y = 10

    def move(self, pl_1, pl_2):
        if self.ycor() >= 300:
            self.step_y = -10
        elif self.ycor() <= -300:
            self.step_y = 10
        elif self.distance(pl_1[0]) <= 15 or self.distance(pl_1[1]) <= 15 or self.distance(pl_1[2]) <= 15:
            self.step_x = -10
        elif self.distance(pl_2[0]) <= 15 or self.distance(pl_2[1]) <= 15 or self.distance(pl_2[2]) <= 15:
            self.step_x = 10
        new_xcor = self.xcor() + self.step_x
        new_ycor = self.ycor() + self.step_y
        self.goto(new_xcor, new_ycor)
