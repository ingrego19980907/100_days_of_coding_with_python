import requests
from datetime import datetime
import smtplib

MY_LAT = 46.650452 # Your latitude
MY_LONG = 32.608181 # Your longitude
my_email = "probmailfortest@gmail.com"
password = 'Admin_123'

response = requests.get(url="http://api.open-notify.org/iss-now.json")
response.raise_for_status()
data = response.json()

iss_latitude = float(data["iss_position"]["latitude"])
iss_longitude = float(data["iss_position"]["longitude"])

parameters = {
    "lat": MY_LAT,
    "lng": MY_LONG,
    "formatted": 0,
}

response = requests.get("https://api.sunrise-sunset.org/json", params=parameters)
response.raise_for_status()
data = response.json()
sunrise = int(data["results"]["sunrise"].split("T")[1].split(":")[0])
sunset = int(data["results"]["sunset"].split("T")[1].split(":")[0])

time_now = datetime.now()
time_now_hour = time_now.hour

if time_now_hour < sunrise or time_now_hour > sunset:
    if int(MY_LAT) == int(iss_latitude) and int(MY_LONG) == int(iss_longitude):
        with smtplib.SMTP('smtp.gmail.com') as connection:
            connection.starttls()
            connection.login(user=my_email, password=password)
            connection.sendmail(
                from_addr=my_email,
                to_addrs='suhodoiev@gmail.com',
                msg=f'Subject:Iss is coming\n\nLook on the sky!'
            )
