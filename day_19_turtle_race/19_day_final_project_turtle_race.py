from turtle import Turtle, Screen
from random import randint

screen = Screen()
screen.setup(width=500, height=400)
user_bet = screen.textinput(title='Make your bet', prompt='Which turtle will win the race? Enter color.')
colors = ['red', 'yellow', 'cyan', 'green', 'purple', 'blue']

turtles = [
    Turtle(shape='turtle'),
    Turtle(shape='turtle'),
    Turtle(shape='turtle'),
    Turtle(shape='turtle'),
    Turtle(shape='turtle'),
    Turtle(shape='turtle')
]

x = -230
y = -100
i = 0
for turtle in turtles:
    turtle.color(colors[i])
    i += 1
    turtle.penup()
    turtle.goto(x, y)
    y += 30

turtle_win = False
while not turtle_win:
    for turtle in turtles:
        turtle.forward(randint(1, 10))
        if turtle.xcor() > 230:
            turtle_win = True
            print(f"{turtle.color()[0]} turtle won!!!")
            if user_bet == turtle.color()[0]:
                print("You've Won!!!")
            else:
                print("You've Lost")



screen.exitonclick()
